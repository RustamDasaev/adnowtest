<?php

class FrontController
{
    private static $_instance = null;
    private $_routes = [];
    private $_params = [];
    private $_controller = 'IndexController';
    private $_action = 'actionNotFound';
    private $_body = null;

    public static function getInstance()
    {
        if (!self::$_instance instanceof self) {

            self::$_instance = new self;
        }

        return self::$_instance;
    }

    protected function __clone()
    {
    }

    protected function __construct()
    {
        $path = explode('?', $_SERVER['REQUEST_URI']);
        $path = array_shift($path);

        if ($path != '/') {
            $path = trim($path, '/');
        }

        foreach ($this->getRoutes() as $pattern) {

            if (preg_match('~^' . $pattern . '$~', $path)) {

                $segments = explode('/', $path);

                $this->_controller = ucfirst(!empty($segments[0]) ? array_shift($segments) : 'Index') . 'Controller';
                $this->_action = 'action' . ucfirst(!empty($segments[0]) ? array_shift($segments) : 'Index');

                if (count($segments) > 0) {
                    $this->_params['id'] = (int)array_shift($segments);
                }

                //Без костылей никак
                if (isset($_GET['title'])) {
                    $this->_params['title'] = $_GET['title'];
                    $this->_params['map'] = $_GET['map'];
                }
            }
        }
    }

    private function getRoutes()
    {
        $this->_routes[] = '/';
        $this->_routes[] = 'api/all';
        $this->_routes[] = 'api/add';
        $this->_routes[] = 'api/get/([0-9]+)';
        $this->_routes[] = 'api/save/([0-9]+)';
        $this->_routes[] = 'api/drop/([0-9]+)';

        return $this->_routes;
    }

    private function getController()
    {
        return $this->_controller;
    }

    private function getAction()
    {
        return $this->_action;
    }

    private function isAjaxRequest()
    {
        return ($this->_controller == 'ApiController');
    }

    public function run()
    {
        if (class_exists($this->getController())) {
            $rc = new ReflectionClass($this->getController());
            if ($rc->hasMethod($this->getAction())) {
                $obj = $rc->newInstance($this->_params);
                $method = $rc->getMethod($this->getAction());
                $this->setBody($method->invoke($obj));
            }
        }
    }

    public function getBody()
    {
        echo $this->isAjaxRequest() ? json_encode($this->_body) : $this->_body;
    }

    public function setBody($result = null)
    {
        $this->_body = $result;
    }
}