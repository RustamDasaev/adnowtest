<?php

class IndexController
{

    public function __construct()
    {
    }

    public function actionIndex()
    {
        include 'views/index.phtml';
    }

    public function actionNotFound()
    {
        header('Status: 404 Not Found');

        echo 'PAGE NOT FOUND!!!';
        exit;
    }

}