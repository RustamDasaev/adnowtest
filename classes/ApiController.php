<?php

class ApiController
{
    private $_model;

    public function __construct(array $args = [])
    {
        $this->_model = new TabModel($args);
    }

    public function actionAll()
    {
        return $this->_model->getTabs();
    }

    public function actionSave()
    {
        return $this->_model->setTab();
    }

    public function actionDrop()
    {
        return $this->_model->dropTab();
    }

    public function actionAdd()
    {
        return $this->_model->setTab();
    }

    public function actionGet()
    {
        return $this->_model->getTab();
    }
}