<?php

class DB
{
    private $_pdo = null;
    private $_config = null;
    protected static $_instance = null;

    public static function getConnection()
    {
        if (!self::$_instance instanceof self) {
            self::$_instance = new self;
        }

        return self::$_instance->_pdo;
    }

    private function getConfig($name)
    {
        if (is_null($this->_config) || is_null($this->_config[$name])) {
            $this->_config = parse_ini_file(DBCONF);
        }

        if (!$this->_config[$name]) {
            return null;
        }

        return $this->_config[$name];
    }

    private function createTables()
    {
        $this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->_pdo->exec('
SET NAMES "UTF8";

CREATE TABLE IF NOT EXISTS tabs(
	id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	title VARCHAR(255) NOT NULL DEFAULT "",
	created DATETIME,
	last_modify TIMESTAMP,
	map TEXT DEFAULT null,
	UNIQUE(title)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT="Мозаики";');

    }

    protected function __construct()
    {
        try {
            $this->_pdo = new PDO($this->getConfig('connect'), $this->getConfig('user'), $this->getConfig('pass'));
            $this->createTables();
        } catch (Exception $e) {
            die($e->getMessage());
        }

    }

    protected function __clone()
    {
    }
}