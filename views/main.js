map = null;
color = ['white', 'red', 'green', 'blue'];
sizeX = 20;
sizeY = 20;

function echoAllTabs() {
    $.ajax({
        url: "api/all/",
        data: {},
        dataType: "json",
        success: function (data) {
            if (data) {
                var tabs = '';

                $.each(data, function () {
                    var row = this;
                    var id = this['id'];

                    delete this['id'];
                    delete this['map'];

                    tabs += "<tr id='" + id + "'>";

                    $.each(row, function () {
                        tabs += "<td style='cursor:pointer;' onclick='editTab(" + id + ")'>" + this + "</td>";
                    });

                    tabs += "<td onclick='kill(" + id + ");' style='cursor:pointer;color:red;font-size:25px;text-align:center;'>X</td></tr>";
                });

                $("#tabs_list").html(tabs);
            }
        },
        cache: false
    });
}

function kill(id) {
    $.ajax({
        url: "api/drop/" + id + "/",
        data: {},
        dataType: "json",
        success: function (data) {
            if (data) {
                $('#' + id).empty().remove();
            }
        }
    });
}

function fillCell(x, y) {
    map[x][y] = map[x][y] == color.length - 1 ? 0 : map[x][y] + 1;
    $("#" + x + '_' + y).css('background-color', color[map[x][y]]);
}

function drawMap() {
    $('#tab_map').html('');

    for (var i = 0; i < sizeX; i++) {
        for (var j = 0; j < sizeY; j++) {
            if (map[i][j] == undefined) {
                map[i][j] = 0;
            }

            $('#tab_map').append('<div id="' + i + '_' + j + '" onclick="fillCell(' + i + ', ' + j + ')" style="float:left;margin:1px;background-color:' + color[map[i][j]] + ';width:20px;height:20px;">');
        }

        $('#tab_map').append('<div style="clear: left;">');
    }
}

function editTab(id) {

    toggleForms();

    $('#id').val('');
    $('#title').val('');
    $('#edit_tab_container h1').html('Создание мозаики');
    map = null;

    if (id !== null) {
        $.ajax({
            url: 'api/get/' + id + '/',
            data: {},
            dataType: "json",
            success: function (data) {
                if (data) {
                    $('#id').val(data['id']);
                    $('#title').val(data['title']);
                    $('#edit_tab_container h1').html('Изменение мозаики');
                    map = JSON.parse(data['map']);
                    drawMap();
                }
            }
        });
    }

    if (map === null) {
        map = new Array(sizeX);
        for (var i = 0; i < sizeY; i++) {
            map[i] = new Array(sizeY);
        }

        drawMap();
    }
}

function toggleForms() {
    $('#tabs_container').toggle();
    $('#edit_tab_container').toggle();
}

$(function () {
    echoAllTabs();

    $('#create_new_tab').click(function () {
        editTab(null);
    });

    $('#cancel').click(function () {
        toggleForms();
    });

    $('#save').click(
        function () {

            var id = $('#id').val();
            var url = 'add/';

            if (id != '') url = 'save/' + id + '/';

            $.ajax({
                url: "api/" + url,
                data: {title: $('#title').val(), map: JSON.stringify(map)},
                dataType: "json",
                success: function (data) {
                    toggleForms();
                    echoAllTabs();
                }
            });
        }
    );
});

