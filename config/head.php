<?php
session_start();
header('Content-type: text/html; charset=utf-8');
ini_set('display_errors', 1);
set_include_path(get_include_path()
    . PATH_SEPARATOR . 'classes'
    . PATH_SEPARATOR . 'models');

error_reporting(E_ALL);

define('DBCONF', $_SERVER['DOCUMENT_ROOT'] . '/config/dbconf.ini');

function __autoload($className)
{
    include $className . '.php';
}
