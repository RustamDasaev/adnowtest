<?php

class TabModel
{
    private $_db;

    private $_id = null;
    private $_title = null;
    private $_map = null;

    public function __construct($args = [])
    {
        $this->_db = DB::getConnection();

        if (isset($args['id'])) {
            $this->_id = $args['id'];
        }

        if (isset($args['title'])) {
            $this->_title = $args['title'];
        }

        if (isset($args['map'])) {
            $this->_map = $args['map'];
        }
    }

    public function getTabs()
    {
        $stmt = $this->_db->prepare('SELECT * FROM tabs ORDER BY created DESC');
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function getTab()
    {
        $stmt = $this->_db->prepare('SELECT * FROM tabs WHERE id=:id');
        $stmt->execute(array(':id' => $this->_id));

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function setTab()
    {
        $query = 'INSERT INTO tabs (id, title, map, created) VALUES (:id, :title, :map, NOW())';

        if ($this->_id) {
            $query = 'UPDATE tabs SET title=:title, map=:map WHERE id=:id';
        }

        $stmt = $this->_db->prepare($query);
        $stmt->execute(array(':title' => $this->_title, ':map' => $this->_map, ':id' => $this->_id));

        return $this->_db->errorCode() === '00000';
    }


    public function dropTab()
    {
        $stmt = $this->_db->prepare('DELETE FROM tabs WHERE id=:id');
        $stmt->execute(array(':id' => $this->_id));

        return true;
    }
}